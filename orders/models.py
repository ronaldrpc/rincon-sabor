from django.db import models
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver


# Create your models here.
class Client(models.Model):
    name = models.CharField(max_length=45)
    last_name = models.CharField(max_length=45)
    observations = models.TextField()

    def __str__(self):
        return self.name


class Waiter(models.Model):
    name = models.CharField(max_length=45)
    last_name_1 = models.CharField(max_length=45)
    last_name_2 = models.CharField(max_length=45)

    def __str__(self):
        return self.name


class Table(models.Model):
    num_diners = models.IntegerField()
    location = models.CharField(max_length=45)

    def __str__(self):
        return f'Mesa {self.id}'


class Invoice(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    client_id = models.ForeignKey(Client, on_delete=models.CASCADE)
    waiter_id = models.ForeignKey(Waiter, on_delete=models.CASCADE)
    table_id = models.ForeignKey(Table, on_delete=models.CASCADE)
    total = models.FloatField(default=0)

    def __str__(self):
        return f'Factura {self.id}'


class ProductStock(models.Model):
    TYPES_OF_PRODUCTS = [
        ('dish', 'Dish'),
        ('drink', 'Drink'),
    ]
    name = models.CharField(max_length=50)
    price = models.FloatField()
    type = models.CharField(max_length=5, choices=TYPES_OF_PRODUCTS)

    def __str__(self):
        return self.name


class ProductRequest(models.Model):
    quantity = models.IntegerField()
    invoice_id = models.ForeignKey(Invoice, on_delete=models.CASCADE)
    product_id = models.ForeignKey(ProductStock, on_delete=models.CASCADE)
    price_request = models.FloatField(default=0)

    def __str__(self):
        return self.product_id.name


@receiver(pre_save, sender=ProductRequest)
def update_price_request(sender, instance, **kwargs):
    instance.price_request = instance.product_id.price * instance.quantity


@receiver(post_save, sender=ProductRequest)
def update_price_invoice(sender, instance, created, **kwargs):
    instance.invoice_id.total += instance.product_id.price * instance.quantity
    instance.invoice_id.save()
