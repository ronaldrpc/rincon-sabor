from rest_framework import serializers
from orders.models import *


class ClientSerializer(serializers.ModelSerializer):
    """ """
    class Meta:
        model = Client
        fields = ['id', 'name', 'last_name', 'observations']


class WaiterSerializer(serializers.ModelSerializer):
    """ """
    class Meta:
        model = Waiter
        fields = ['id', 'name', 'last_name_1', 'last_name_2']


class TableSerializer(serializers.ModelSerializer):
    """ """
    class Meta:
        model = Table
        fields = ['id', 'num_diners', 'location']


class ProductStockSerializer(serializers.ModelSerializer):
    """ """
    class Meta:
        model = ProductStock
        fields = ['id', 'name', 'price', 'type']


class ProductRequestSerializer(serializers.ModelSerializer):
    """ """
    class Meta:
        model = ProductRequest
        fields = ['id', 'quantity', 'invoice_id', 'drink_id']


class InvoiceSerializer(serializers.ModelSerializer):
    """ """
    product_set = ProductRequestSerializer(read_only=True, many=True)

    class Meta:
        model = Invoice
        fields = ['id', 'date', 'client_id', 'waiter_id', 'table_id', 'product_set']

    def to_representation(self, instance):
        self.fields['client_id'] = ClientSerializer(read_only=True)
        self.fields['waiter_id'] = WaiterSerializer(read_only=True)
        self.fields['table_id'] = TableSerializer(read_only=True)
        return super().to_representation(instance)
