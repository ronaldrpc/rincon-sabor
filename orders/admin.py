from django.contrib import admin
from orders.models import *


# Register your models here.

@admin.register(Invoice)
class InvoiceAdmin(admin.ModelAdmin):
    list_display = ['id', 'date', 'client_id', 'waiter_id', 'table_id', 'total']


@admin.register(ProductRequest)
class ProductRequestAdmin(admin.ModelAdmin):
    list_display = ['id', 'quantity', 'invoice_id', 'product_id', 'price_request']


@admin.register(Table)
class TableAdmin(admin.ModelAdmin):
    list_display = ["id", 'num_diners', 'location']


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = ["id", 'name', 'last_name', 'observations']


@admin.register(Waiter)
class WaiterAdmin(admin.ModelAdmin):
    list_display = ["id", 'name', 'last_name_1', 'last_name_2']


@admin.register(ProductStock)
class DishStockAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'price', 'type']
