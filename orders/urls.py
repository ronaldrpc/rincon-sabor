from django.urls import path, include
from rest_framework.routers import DefaultRouter, SimpleRouter

from orders import views

# Use DefaultRouter to see API root view (urls)
router = DefaultRouter()
router.register(r'invoice', views.InvoiceViewSet, basename="invoice")
router.register(r'client', views.ClientViewSet, basename="client")
router.register(r'waiter', views.WaiterViewSet, basename="waiter")
router.register(r'table', views.TableViewSet, basename="table")
router.register(r'product-request', views.ProductRequestViewSet, basename="product-request")
router.register(r'stock', views.ProductStockViewSet, basename="stock")

# urlpatterns = router.urls
urlpatterns = [
    path('', include(router.urls)),
]





